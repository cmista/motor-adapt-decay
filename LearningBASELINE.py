import pickle
import numpy as np
import os
import mne
import h5py
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import pyplot as plt
from mne.stats import (spatio_temporal_cluster_1samp_test, summarize_clusters_stc)
from mne.datasets import (sample, fetch_fsaverage)
import seaborn as sns
from scipy import stats
from mne.stats import bonferroni_correction, fdr_correction

montage = mne.channels.make_standard_montage('biosemi64')
montage.ch_names = [element.upper() for element in montage.ch_names]

# Create some dummy metadata
n_channels = 60
channels_name = ['FP1', 'FPZ', 'FP2', 'AF7', 'AF3', 'AF4', 'AF8', 'F7', 'F5', 'F3', 'F1', 'FZ', 'F2', 'F4', 'F6', 'F8',
                 'FT7', 'FC5', 'FC3', 'FC1', 'FCZ', 'FC2', 'FC4', 'FC6', 'FT8', 'T7', 'C5', 'C3', 'C1', 'CZ', 'C2', 'C4',
                 'C6', 'T8', 'TP7', 'CP5', 'CP3', 'CP1', 'CPZ', 'CP2', 'CP4', 'CP6', 'TP8', 'P7', 'P5', 'P3', 'P1', 'PZ',
                 'P2', 'P4', 'P6', 'P8', 'PO7', 'PO3', 'POZ', 'PO4', 'PO8', 'O1', 'OZ', 'O2']

indexes ={str(k):element for element,k in zip(channels_name, range(60))}

sampling_freq = 500  # in Hertz
info = mne.create_info(n_channels, sfreq=sampling_freq, ch_types='eeg')

filepath_nf = './data/merged_epochs nf BASELINE.mat'
f = h5py.File(filepath_nf)
for k, v in f.items():
    data = np.array(v)

filepath_f = './data/merged_epochs fb BASELINE.mat'
f = h5py.File(filepath_f)
for k, v in f.items():
    dataf = np.array(v)

# feedback vs no feedback
data = np.squeeze(data).T
dataf = np.squeeze(dataf).T


epochs = mne.EpochsArray(data, info=info, tmin=-3)
epochs.rename_channels(indexes)

epochsf = mne.EpochsArray(dataf, info=info, tmin=-3)
epochsf.rename_channels(indexes)

epochs.set_montage(montage)
epochsf.set_montage(montage)


times = np.arange(-0.5, 0.5, 0.1)
#times = [-0.05, 0, 0.05, 0.1, 0.2, 0.3, 0.5]
headplot = epochs.average()
headplotf = epochsf.average()

#fig,ax = plt.subplots(2,11)
headplot.plot_topomap(times,  ch_type='eeg',title= "Without visual feedback",
                                                  time_unit='s',
                      cmap='Spectral_r', colorbar = True)

mpl.rcParams['text.usetex'] = False
mpl.rcParams['svg.fonttype'] = 'none'

plt.savefig('headpltbasenf.svg', facecolor='w', edgecolor='w',dpi=200,
        orientation='portrait', format='svg',
        transparent=False, bbox_inches='tight', pad_inches=0.1,
        metadata=None)

#ax[1].set_title("Without visual feedback")
# ax[0].set_ylim([0,0.05])
mpl.rcParams['text.usetex'] = False
mpl.rcParams['svg.fonttype'] = 'none'

headplotf.plot_topomap(times, ch_type='eeg', title= "With visual "
                                                    "feedback",
                       time_unit='s', cmap='Spectral_r', colorbar = True)

plt.savefig('headpltbasetf.svg', facecolor='w', edgecolor='w',dpi=200,
        orientation='portrait', format='svg',
        transparent=False, bbox_inches='tight', pad_inches=0.1,
        metadata=None)

#ax[0].set_title("With visual feedback")
plt.show()


# epochs.plot_image(picks=['CZ', 'FPZ'])
channel = ['CZ']
feed = np.squeeze(epochs.get_data(picks=channel))
nofeed = np.squeeze(epochsf.get_data(picks=channel))

times = epochs.times

tvalues, pvalues = stats.ttest_rel(feed, nofeed, axis=0) #test 1
alpha = 0.05     #test 1
reject_fdr, pval_fdr = mne.stats.fdr_correction(pvalues, alpha=alpha) #test 1

# Get color palette
palette = sns.color_palette()

# test 1
fig,ax = plt.subplots(2)
ax[0].plot(times, np.squeeze(pvalues), color=palette[0])
ax[0].plot(times, np.squeeze(pval_fdr), color=palette[1])
ax[0].set_title(channel[0])
# ax[0].set_ylim([0,0.05])
ax[1].plot(times, np.squeeze(tvalues), color=palette[2])
plt.show()

#time windows spatial analysis #test 2
temporal_mask = np.logical_and(0 <= times, times <= 0.4)
dataAVG = np.mean(data[:, :, temporal_mask], axis=2)
dataAVG_f = np.mean(dataf[:, :, temporal_mask], axis=2)
contrast = dataAVG-dataAVG_f
n_permutations = 10000 #Exact p values

T0, p_values, H0 = mne.stats.permutation_t_test(contrast, n_permutations, n_jobs=1)

evoked = mne.EvokedArray(-np.log10(p_values)[:, np.newaxis], epochs.info, tmin=0.)
mask = p_values[:, np.newaxis] <= 0.05

evoked.plot_topomap(ch_type='eeg', times=[0], scalings=1, res=100,
                    time_format=None, cmap='Reds', vmin=0.5, vmax=np.max,
                    units='-log10(p)', cbar_fmt='-%0.1f', mask=mask,show_names=True,
                    size=10, time_unit='s')


# Calculate cluster time-spatial cluster #test 3
# adjacency matrix between sensors from their locations
adjacency, _ = mne.channels.find_ch_adjacency(epochs.info, "eeg")

X = epochs.get_data().transpose(0, 2, 1)- epochsf.get_data().transpose(0, 2, 1)

# tfce = dict(start=.2, step=.2)
threshold = 4

# Calculate statistical thresholds
t_obs, clusters, cluster_pv, h0 = mne.stats.spatio_temporal_cluster_1samp_test( X,  threshold=threshold, adjacency=adjacency, n_jobs= 3,
    n_permutations=1000, step_down_p=0.05)  # a more standard number would be 1000+
significant_points = np.where(cluster_pv < 0.05)[0]
