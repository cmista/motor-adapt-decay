import pickle
import numpy as np
import os
import mne
import h5py
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import pyplot as plt
from mne.stats import (spatio_temporal_cluster_1samp_test, summarize_clusters_stc)
from mne.datasets import (sample, fetch_fsaverage)
import seaborn as sns
from scipy import stats
from mne.stats import bonferroni_correction, fdr_correction

montage = mne.channels.make_standard_montage('biosemi64')
montage.ch_names = [element.upper() for element in montage.ch_names]

# Create some dummy metadata
n_channels = 60
channels_name = ['FP1', 'FPZ', 'FP2', 'AF7', 'AF3', 'AF4', 'AF8', 'F7', 'F5', 'F3', 'F1', 'FZ', 'F2', 'F4', 'F6', 'F8',
                 'FT7', 'FC5', 'FC3', 'FC1', 'FCZ', 'FC2', 'FC4', 'FC6', 'FT8', 'T7', 'C5', 'C3', 'C1', 'CZ', 'C2', 'C4',
                 'C6', 'T8', 'TP7', 'CP5', 'CP3', 'CP1', 'CPZ', 'CP2', 'CP4', 'CP6', 'TP8', 'P7', 'P5', 'P3', 'P1', 'PZ',
                 'P2', 'P4', 'P6', 'P8', 'PO7', 'PO3', 'POZ', 'PO4', 'PO8', 'O1', 'OZ', 'O2']

indexes ={str(k):element for element,k in zip(channels_name, range(60))}

sampling_freq = 500  # in Hertz
info = mne.create_info(n_channels, sfreq=sampling_freq, ch_types='eeg')

files = ['./data/merged_epochs nf BASELINE.mat',
         './data/merged_epochs fb BASELINE.mat',
         './data/merged_epochs nf ADAPTATION.mat',
         './data/merged_epochs fb ADAPTATION.mat',
         './data/merged_epochs nf RETENTION.mat',
         './data/merged_epochs fb RETENTION.mat',
         ]

data = []

for filepath in files:
    f = h5py.File(filepath)
    for k, v in f.items():
        data.append(np.array(v))

# feedback vs no feedback new axis  condition x  subject x channels x time
data = np.squeeze(data)
data = np.transpose(data,(0,3,2,1))

epochs = [mne.EpochsArray(condition, info=info, tmin=-3) for condition in data]
epochs = [condition.rename_channels(indexes) for condition in epochs]
epochs = [condition.set_montage(montage) for condition in epochs]

times = epochs[0].times

# Get color palette
palette = sns.color_palette()

# Tweak the figure style
mpl.rcParams.update({
    'ytick.labelsize': 18,
    'xtick.labelsize': 18,
    'axes.labelsize': 20,
    'axes.titlesize': 20,
})

fig,ax = plt.subplots(2,3, gridspec_kw={'hspace': 0.08, 'wspace': 0.1}, figsize=(18, 6))
sns.despine(right='False', top='False')

style = dict(size=14, color='black')
for row in ax:
    for elem in row:
        elem.text(0, 4, "MOV INIT", ha='center',  **style)
        elem.axvline(ymax=0.8, linewidth=2, color=palette[3], dashes=(2,2), alpha= 0.6)
        elem.set_ylim([-8, 8])
        elem.set_xlim([-3, 3])
        elem.set_xticks(np.arange(-2,3), minor=False)

# Hide x labels and tick labels for top plots and y ticks for right plots.
for axs in ax.flat:
    axs.label_outer()

# Text on figure
ax[0, 0].set_title('Baseline')
ax[0, 1].set_title('Adaptation')
ax[0, 2].set_title('Deadaptation')
ax[0, 0].set_ylabel('Amplitude (μv)')
ax[1, 0].set_ylabel('Amplitude (μv)')
ax[1, 0].set_xlabel('Time (s)')
ax[1, 1].set_xlabel('Time (s)')
ax[1, 2].set_xlabel('Time (s)')

# set legend
feed_patch = mpl.patches.Patch(color=palette[1], label='With feedback')
Nofeed_patch = mpl.patches.Patch(color=palette[0], label='Without feedback')
ax[0,2].legend(handles=[feed_patch, Nofeed_patch], handletextpad=0.4,fontsize=14, loc='upper right', frameon=False)

# For channel CZ
channel = ['FZ']
at = mpl.offsetbox.AnchoredText(channel[0],
                  prop=dict(size=18, weight='bold'), frameon=True,
                  loc='lower left')

at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
ax[0,0].add_artist(at)

feed = np.squeeze(epochs[0].get_data(picks=channel)).mean(axis=0)
nofeed = np.squeeze(epochs[1].get_data(picks=channel)).mean(axis=0)
feedSD = np.squeeze(epochs[0].get_data(picks=channel)).std(axis=0)
nofeedSD = np.squeeze(epochs[1].get_data(picks=channel)).std(axis=0)

ax[0, 0].plot(times, feed, color=palette[0])
ax[0, 0].plot(times, nofeed, color=palette[1])
ax[0, 0].fill_between(times, feed+feedSD,feed-feedSD, color=palette[0],
                      alpha=0.3)
ax[0, 0].fill_between(times, nofeed+nofeedSD,nofeed-nofeedSD, color=palette[1],
                      alpha=0.3)

feed = np.squeeze(epochs[2].get_data(picks=channel)).mean(axis=0)
nofeed = np.squeeze(epochs[3].get_data(picks=channel)).mean(axis=0)
feedSD = np.squeeze(epochs[2].get_data(picks=channel)).std(axis=0)
nofeedSD = np.squeeze(epochs[3].get_data(picks=channel)).std(axis=0)

ax[0, 1].plot(times, feed, color=palette[0])
ax[0, 1].plot(times, nofeed, color=palette[1])
ax[0, 1].fill_between(times, feed+feedSD,feed-feedSD, color=palette[0],
                      alpha=0.3)
ax[0, 1].fill_between(times, nofeed+nofeedSD,nofeed-nofeedSD, color=palette[1],
                      alpha=0.3)

feed = np.squeeze(epochs[4].get_data(picks=channel)).mean(axis=0)
nofeed = np.squeeze(epochs[5].get_data(picks=channel)).mean(axis=0)
feedSD = np.squeeze(epochs[4].get_data(picks=channel)).std(axis=0)
nofeedSD = np.squeeze(epochs[5].get_data(picks=channel)).std(axis=0)

ax[0, 2].plot(times, feed, color=palette[0])
ax[0, 2].plot(times, nofeed, color=palette[1])
ax[0, 2].fill_between(times, feed+feedSD,feed-feedSD, color=palette[0],
                      alpha=0.3)
ax[0, 2].fill_between(times, nofeed+nofeedSD,nofeed-nofeedSD, color=palette[1],
                      alpha=0.3)

# calculating difference between conditions
feed = np.squeeze(epochs[4].get_data(picks=channel))
nofeed = np.squeeze(epochs[5].get_data(picks=channel))

t_obs, clusters, cluster_pv, H0 = clu =\
    mne.stats.permutation_cluster_1samp_test(feed-nofeed, n_permutations=1000, tail=0, n_jobs=1, adjacency=None,
                             out_type='mask')

for i_c, c in enumerate(clusters):
    c = c[0]
    if cluster_pv[i_c] <= 0.05:
        h = ax[0, 2].axvspan(times[c.start], times[c.stop - 1], ymax=0.8,
                        color='r', alpha=0.3)

# For channel CZ
channel = ['CZ']
at = mpl.offsetbox.AnchoredText(channel[0],
                  prop=dict(size=14, weight='bold'), frameon=True,
                  loc='lower left')

at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
ax[1,0].add_artist(at)

feed = np.squeeze(epochs[0].get_data(picks=channel)).mean(axis=0)
nofeed = np.squeeze(epochs[1].get_data(picks=channel)).mean(axis=0)

feedSD = np.squeeze(epochs[0].get_data(picks=channel)).std(axis=0)
nofeedSD = np.squeeze(epochs[1].get_data(picks=channel)).std(axis=0)

ax[1, 0].plot(times, feed, color=palette[0])
ax[1, 0].fill_between(times, feed+feedSD,feed-feedSD, color=palette[0],
                      alpha=0.3)
ax[1, 0].plot(times, nofeed, color=palette[1])
ax[1, 0].fill_between(times, nofeed+nofeedSD,nofeed-nofeedSD, color=palette[1],
                      alpha=0.3)

feed = np.squeeze(epochs[2].get_data(picks=channel)).mean(axis=0)
nofeed = np.squeeze(epochs[2].get_data(picks=channel)).mean(axis=0)

nofeed = np.squeeze(epochs[3].get_data(picks=channel)).mean(axis=0)
nofeedSD = np.squeeze(epochs[3].get_data(picks=channel)).std(axis=0)

ax[1, 1].plot(times, feed, color=palette[0])
ax[1, 1].fill_between(times, feed+feedSD,feed-feedSD, color=palette[0],
                      alpha=0.3)

ax[1, 1].plot(times, nofeed, color=palette[1])
ax[1, 1].fill_between(times, nofeed+nofeedSD,nofeed-nofeedSD, color=palette[1],
                      alpha=0.3)


feed = np.squeeze(epochs[4].get_data(picks=channel)).mean(axis=0)
feed = np.squeeze(epochs[4].get_data(picks=channel)).mean(axis=0)

nofeed = np.squeeze(epochs[5].get_data(picks=channel)).mean(axis=0)
nofeed = np.squeeze(epochs[5].get_data(picks=channel)).mean(axis=0)

ax[1, 2].plot(times, feed, color=palette[0])
ax[1, 2].fill_between(times, feed+feedSD,feed-feedSD, color=palette[0],
                      alpha=0.3)

ax[1, 2].plot(times, nofeed, color=palette[1])
ax[1, 2].fill_between(times, nofeed+nofeedSD,nofeed-nofeedSD, color=palette[1],
                      alpha=0.3)

# ax[0,2].legend((p1[0], p2[0]), ('With feedback', 'Without feedback'))


feed = np.squeeze(epochs[4].get_data(picks=channel))
nofeed = np.squeeze(epochs[5].get_data(picks=channel))

t_obs, clusters, cluster_pv, H0 = clu =\
    mne.stats.permutation_cluster_1samp_test(feed-nofeed, n_permutations=1000, tail=0, n_jobs=1, adjacency=None,
                             out_type='mask')

for i_c, c in enumerate(clusters):
    c = c[0]
    if cluster_pv[i_c] <= 0.05:
        h = ax[1, 2].axvspan(times[c.start], times[c.stop - 1], ymax=0.8,
                        color='r', alpha=0.3)
    # else:
        # ax[1, 2].axvspan(times[c.start], times[c.stop - 1], color=(0.3, 0.3, 0.3), alpha=0.3)

plt.show()

True



